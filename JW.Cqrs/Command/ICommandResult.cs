﻿namespace JW.Cqrs.Command
{
    /// <summary>
    /// Command result marker.
    /// </summary>
    public interface ICommandResult
    {
        bool IsSuccess { get; }
        string Message { get; set; }
    }
}
