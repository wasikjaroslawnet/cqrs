﻿namespace JW.Cqrs.Command
{
    /// <summary>
    /// Command result without additional data.
    /// </summary>
    public class CommandResult : ICommandResult
    {
        public CommandResult(bool isSuccess)
        {
            IsSuccess = isSuccess;
        }

        public bool IsSuccess { get; }
        public string Message { get; set; }
    }

    /// <summary>
    /// Command result with additional data.
    /// </summary>
    /// <typeparam name="TData">
    /// Type of additional data.
    /// </typeparam>
    public class CommandResult<TData> : ICommandResult
    {
        public CommandResult(bool isSuccess)
        {
            IsSuccess = isSuccess;
        }

        public bool IsSuccess { get; }
        public string Message { get; set; }
        public TData Data { get; set; }
    }
}
