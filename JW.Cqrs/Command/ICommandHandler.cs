﻿namespace JW.Cqrs.Command
{
    /// <summary>
    /// Command hanlder marker.
    /// </summary>
    /// <typeparam name="TParameter">
    /// Command type.
    /// </typeparam>
    /// <typeparam name="TResult">
    /// Command result type.
    /// </typeparam>
    public interface ICommandHandler<in TParameter, TResult>
        where TParameter : ICommand where TResult : ICommandResult
    {
    }
}
