﻿namespace JW.Cqrs.Command
{
    /// <summary>
    /// Command marker.
    /// </summary>
    public interface ICommand
    {
    }
}
