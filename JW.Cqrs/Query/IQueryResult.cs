﻿namespace JW.Cqrs.Query
{
    /// <summary>
    /// Query result marker.
    /// </summary>
    public interface IQueryResult
    {
    }
}
