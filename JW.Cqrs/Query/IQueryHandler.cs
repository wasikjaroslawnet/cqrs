﻿using System.Threading.Tasks;

namespace JW.Cqrs.Query
{
    /// <summary>
    /// Query hanlder marker.
    /// </summary>
    /// <typeparam name="TParameter">
    /// Query type.
    /// </typeparam>
    /// <typeparam name="TResult">
    /// Query result type.
    /// </typeparam>
    public interface IQueryHandler<in TParameter, TResult>
        where TParameter : IQuery where TResult : IQueryResult
    {
        /// <summary>
        /// Retrieve result from query.
        /// </summary>
        /// <param name="query">
        /// Query object wiht optional parameters.
        /// </param>
        /// <returns>
        /// Query result.
        /// </returns>
        Task<TResult> Retrieve(TParameter query);
    }
}
