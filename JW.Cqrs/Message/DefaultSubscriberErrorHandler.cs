using System;

namespace JW.Cqrs.Message
{
    public class DefaultSubscriberErrorHandler : ISubscriberErrorHandler
    {
        public void Handle(IMessage message, Exception exception)
        {
            //default behaviour is to do nothing

        }
    }
}