using System;

namespace JW.Cqrs.Message
{
    public interface ISubscriberErrorHandler
    {
        void Handle(IMessage message, Exception exception);
    }
}